<?php
/*
    #Validamos que exista una sesión
    session_start();
    if(empty($_GET['cuenta'])){
            header("Location: login.php");
    }
    */
?>

<html>
<html lang="es"> 
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" type="text/css" href="styles.css">
        <title> Formulario </title>
        <!-- CSS only -->
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-Zenh87qX5JnK2Jl0vWa8Ck2rdkQ2Bzep5IDxbcnCeuOxjzrPF/et3URy9Bv1WTRi" crossorigin="anonymous">
    </head>
	<body>
    <nav class="navbar navbar-expand-lg bg-light">
            <div class="container-fluid">
                <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>
                <div class="collapse navbar-collapse" id="navbarNav">
                    <ul class="navbar-nav">
                        <li class="nav-item">
                        <a class="nav-link active" aria-current="page" href="info.php">Home</a>
                        </li>
                        <li class="nav-item">
                        <a class="nav-link" href="formulario.php">Registrar alumnos</a>
                        </li>
                        <li class="nav-item">
                        <a class="nav-link" href="cerrarSesion.php">Cerrar sesión</a>
                        </li>
                    </ul>
                </div>
            </div>
        </nav>
		<div class="boxx">
		<method="POST" action="usuario.php">
            <div class="row mb-3">
                <label>Número de cuenta</label>
                <div class="col-sm-10">
                <input type="integer" class="form-control" id="numeroCuenta">
                </div>
            </div>
            <div class="row mb-3">
                <label>Nombre</label>
                <div class="col-sm-10">
                <input type="string" class="form-control" id="nombreF">
                </div>
            </div>
            <div class="row mb-3">
                <label>Primer apellido</label>
                <div class="col-sm-10">
                <input type="string" class="form-control" id="apellido1F">
                </div>
            </div>
            <div class="row mb-3">
                <label>Segundo apellido</label>
                <div class="col-sm-10">
                <input type="string" class="form-control" id="apellido2F">
                </div>
            </div>

            <fieldset class="row mb-3">
                <legend class="col-form-label col-sm-2 pt-0">Género</legend>
                <div class="col-sm-10">
                <div class="form-check">
                    <input class="form-check-input" type="radio" name="Hombre" id="generoF" value="option1" checked>
                    <label class="form-check-label" for="gridRadios1">
                    Hombre
                    </label>
                </div>
                <div class="form-check">
                    <input class="form-check-input" type="radio" name="Mujer" id="generoF" value="option2">
                    <label class="form-check-label" for="gridRadios2">
                    Mujer
                    </label>
                </div>
                <div class="form-check">
                    <input class="form-check-input" type="radio" name="Otro" id="generoF" value="option3">
                    <label class="form-check-label" for="gridRadios3">
                    Otro
                    </label>
                </div>
                </div>
            </fieldset>
            <div class="row mb-3">
                <label class="form-label" for="input-date">Fecha de Nacimiento</label>
                <div class="col-sm-10">
                <input name="date" class="form-control" type="date" id="fechaNacimientoF" placeholder="fec_nac">
                </div>
            </div>

            <div class="row mb-3">
                <label>Contraseña</label>
                <div class="col-sm-10">
                <input type="password" class="form-control" id="contrasenaF">
                </div>
            </div>

            <button type="submit" class="btn btn-primary">Registrar</button>
            <input type='reset' class="btn btn-primary" value="limpiar"/>
        </form>
		</div>
	</body>
</html>