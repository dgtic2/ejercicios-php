<?php
    #Validamos que exista una sesión
    session_start();
    if(empty($_GET['cuenta'])){
            header("Location: login.php");
    }
?>
<html>
<html lang="es"> 
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" type="text/css" href="styles.css">
        <title> Inicio de sesión </title>
        <!-- CSS only -->
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-Zenh87qX5JnK2Jl0vWa8Ck2rdkQ2Bzep5IDxbcnCeuOxjzrPF/et3URy9Bv1WTRi" crossorigin="anonymous">
    </head>
    <body>
        <nav class="navbar navbar-expand-lg bg-light">
            <div class="container-fluid">
                <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>
                <div class="collapse navbar-collapse" id="navbarNav">
                    <ul class="navbar-nav">
                        <li class="nav-item">
                        <a class="nav-link active" aria-current="page" href="info.php">Home</a>
                        </li>
                        <li class="nav-item">
                        <a class="nav-link" href="formulario.php">Registrar alumnos</a>
                        </li>
                        <li class="nav-item">
                        <a class="nav-link" href="cerrarSesion.php">Cerrar sesión</a>
                        </li>
                    </ul>
                </div>
            </div>
        </nav>
        <div class"container">
            <ul class="list-group">
                <li class="list-group-item active" aria-current="true">Usuario Autenticado</li>
                <li class="list-group-item">Nombre: <?php echo $_SESSION['Alumno'][$_GET['cuenta']]['nombre'];?></li>
                <li class="list-group-item">Número de cuenta: <?php echo $_SESSION['Alumno'][$_GET['cuenta']]['num_cta'];?> </li>
                <li class="list-group-item">Fecha de nacimiento: <?php echo $_SESSION['Alumno'][$_GET['cuenta']]['fec_nac'];?> </li>
                <li class="list-group-item active" aria-current="true">Información</li>
            </ul>

            <ul class="list-group list-group-horizontal-xxl">
                <li class="list-group-item">Datos guardados:</li>
            </ul>

            <ul class="list-group list-group-horizontal">
                <li class="list-group-item">#</li>
                <li class="list-group-item">Nombre</li>
                <li class="list-group-item">Fecha de nacimiento</li>
                
            </ul>

        </div>
    </body>

</html>