<html>
    <html lang="es"> 
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" type="text/css" href="styles.css">
        <title> Inicio de sesión </title>
        <!-- CSS only -->
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-Zenh87qX5JnK2Jl0vWa8Ck2rdkQ2Bzep5IDxbcnCeuOxjzrPF/et3URy9Bv1WTRi" crossorigin="anonymous">
    </head>

    <body>
        <nav class="navbar bg-light">
            <div class="container-fluid">
                <a class="navbar-brand" href="https://www.flaticon.es/icono-gratis/usuario_1177568?related_id=1177568">
                <img src="" alt="Logo" width="30" height="24" class="d-inline-block align-text-top">
                Bienvenido al sistema de registro de alumnos de la DGTIC
                </a>
            </div>
        </nav>
        <div class="box">
            <form method="POST" action="usuario.php">
                <p>Login</p>
                <div class="mb-3">
                    <label>Número de cuenta:</label>
                    <input name="cuenta" type="text" class="form-control" placeholder="xxxxxxxxx">
                    <div id="emailHelp" class="form-text">We'll never share your email with anyone else.</div>
                </div>
                <div class="mb-3">
                    <label>Contraseña:</label>
                    <input name="contrasena" type="password" class="form-control">
                </div>
                <!-- Muestra un mensaje de error si el usuario no ingreso correctamente sus datos o hay campos vacíos -->
                <?php
                    if(isset($_GET['error'])){
                        $error = $_GET['error'];
                        if($error == "usuContra"){
                            echo "<h3> El usuario o contraseña no son correctos</h3>";
                        }
                        if($error=="camVacio"){
                            echo "<h3>Lo campos están vacíos</h3>";
                        }
                    }
                ?>
                <button type="submit" class="btn btn-primary">Entrar</button>
            </form>
        </div>
    </body>
</html>