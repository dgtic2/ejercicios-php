<?php    
    # Validar con expresión regular el ingreso de un correo electronico
    # Variable que contiene la expresión regular para correo electronico
    $email="/\b[A-Za-z0-9._%+-]+@[A-Za-z0-9.-]+\.[A-Za-z]{2,6}\b/";

    if(empty($_POST['correo'])){
        echo "Debe ingresar un correo electrónico <br/>";

    }elseif(preg_match($email, $_POST['correo'])){
        echo "En correo ingresado es valido <br/>";

    }else{
        echo"Ingrese nuevamente el correo electrónico <br/>";
    }

    #Validar con expresión regular el ingreso de un texto con más de 50 caracteres
    # Variable que contiene la expresión regular para validar texto con más de 50 caracteres
    $longitud="/\b\w[a-zA-Z]{50,}/";

    if(empty($_POST['palabra'])){
        echo "Debe ingresar una palabra <br/>";

    }elseif(preg_match($longitud, $_POST['palabra'])){
        echo "La palabra ingresada tiene más de 50 caracteres <br/>";
    }else{
        echo "La palabra ingresada  NO tiene más de 50 caracteres <br/>";
    }

    #Validar con una expresión el ingreso de números y detectar solo números decimales.
    # Variable que contiene la expresión regular para validar solo números decimales
    $decimales = "/\d*[0-9]+\.([0-9]+)/";

    if(empty($_POST['decimal'])){
        echo"Debe ingresar una número decimal <br/>";

    }elseif(preg_match($decimales, $_POST['decimal'])){
        echo "El numero ingresado es un número decimal <br/>";
    }else{
        echo "El numero ingresado No es un número decimal <br/>";
    }

    #Validar con una expresión el ingreso de números y detectar solo los caracteres ABCD123456EFGHIJ78.
    # Variable que contiene la expresión regular para validar solo ABCD123456EFGHIJ78
    $caracteresEspeciales = "/[A-D 123456 E-J 78]/";

    if(empty($_POST['caracteres'])){
        echo "Debe ingresar caracteres<br/>";

    }elseif(preg_match($caracteresEspeciales, $_POST['caracteres'])){
        echo "La palabra ingresada tiene los carracteres indicados<br/>";
    }else{
        echo "La palabra ingresada tiene carracteres no validos para este ejercicio<br/>";
    }


    
?>

