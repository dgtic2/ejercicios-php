<html>
<html lang="es"> 
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" type="text/css" href="styles.css">
        <title> Validar con expresiones regulares</title>
        <!-- Santillán García Angélica -->
        <!-- CSS only -->
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-Zenh87qX5JnK2Jl0vWa8Ck2rdkQ2Bzep5IDxbcnCeuOxjzrPF/et3URy9Bv1WTRi" crossorigin="anonymous">
    </head>
    <body>
        <div class="container-sm">
            <nav class="navbar bg-light">
                <div class="container-fluid">
                    <span class="navbar-text">
                    Ejercicios con expresiones regulares
                    </span>
                </div>
             </nav>
            <form method="POST" action="expresionesRegulares.php">
                <div class="mb-3">
                        <label>Ingrese correo:</label>
                        <input name="correo" type="email" class="form-control">
                        </div>
                <div class="mb-3">
                        <label>Ingrese una palabra:</label>
                        <input name="palabra" type="text" class="form-control">   
                </div>
                <div class="mb-3">
                        <label>Ingrese un número decimal:</label>
                        <input name="decimal" type="" class="form-control">   
                </div>
                <div class="mb-3">
                        <label>Ingrese una palabra que tenga solo estos caracteres ABCD123456EFGHIJ78:</label>
                        <input name="caracteres" type="" class="form-control">   
                </div>
                <button type="submit" class="btn btn-primary">Entrar</button>
            </form>
        <div class="container">
    </body>
</html>

