<html>
    <!--Autor: Santillán García Angélica
        Curso: Programación con php
        Fecha: 12 de noviembre del 2022-->
    <head>
        <title> Pirámide y rombo con php</title>
    </head>
    <body>
            <?php
            echo "<center>";
            /*Variable constante*/
            define('numero', '30');
            define('rombo', '0');

            /*Funcion para construir una piramide */
            function piramide(){
                /*Este bloque imprime una piramide*/
                for ($i = 0; $i <=numero; $i++){
                    $j = 0;
                    while ($j < $i ){
                        echo "*";
                        $j++;
                    }
                    echo "<br />";
                }
            }

             /*Funcion para construir un rombo*/
            function rombo(){
                /*Utilizamos la función piramide para construir el rombo */
                piramide();
                /*Este bloque imprime la parte de abajo del rombo*/
                for ($i = 30; $i >= rombo; $i--){
                    $j = 0;
                    while ($j < $i ){
                        echo "*";
                        $j++;
                    }
                    echo "<br />";
                }

            }
            piramide();
            rombo();
            ?>
    </body>
</html>